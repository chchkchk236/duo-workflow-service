# Duo Workflow Service

## Quick start

* Install dependencies: `poetry install`
* Run service: `poetry run python -m server`
* Run tests: `make test`
* Run linter: `make lint`
* Generate protobuf: `make gen-proto`

## Architecture

https://docs.gitlab.com/ee/architecture/blueprints/duo_workflow/