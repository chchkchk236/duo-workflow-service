import asyncio
import logging
import os
from typing import AsyncIterable

import grpc
from grpc_reflection.v1alpha import reflection

from contract import contract_pb2, contract_pb2_grpc


class DuoWorkflowService(contract_pb2_grpc.DuoWorkflowServicer):
    # pylint: disable=invalid-overridden-method
    async def ExecuteWorkflow(
        self,
        request_iterator: AsyncIterable[contract_pb2.ClientEvent],
        context: grpc.ServicerContext,
    ) -> AsyncIterable[contract_pb2.Action]:
        # Fetch the start workflow call
        start_workflow_request = await anext(aiter(request_iterator))
        logging.info("Starting workflow %s", start_workflow_request)
        # TODO: Connect this to duo_workflow_service
        yield contract_pb2.Action(
            runCommand=contract_pb2.RunCommandAction(command="ls")
        )

    # pylint: enable=invalid-overridden-method


async def serve(port: int) -> None:
    server = grpc.aio.server()
    contract_pb2_grpc.add_DuoWorkflowServicer_to_server(DuoWorkflowService(), server)
    server.add_insecure_port(f"[::]:{port}")
    # enable reflection for faster local development and debugging
    # this can be removed when we are closer to production
    service_names = (
        contract_pb2.DESCRIPTOR.services_by_name["DuoWorkflow"].full_name,
        reflection.SERVICE_NAME,
    )
    reflection.enable_server_reflection(service_names, server)
    logging.info("Starting server on port %d", port)
    await server.start()
    logging.info("Started server")
    await server.wait_for_termination()


def run():
    logging.basicConfig(level=logging.INFO)
    port = int(os.environ.get("PORT", "50052"))
    asyncio.get_event_loop().run_until_complete(serve(port))


if __name__ == "__main__":
    run()
